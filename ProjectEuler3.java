class ProjectEuler3{
    public static void main(String [] args){
        int integer = 2;
        long number = 600851475143L;
        int largestprimefactor = 1;
        for(;integer < Math.sqrt(number);integer++){
            //判断integer是否是prime
            boolean integerisprime = false;
            int temp = 2;
            for(;temp < Math.sqrt(integer);temp++){
                if(integer%temp == 0){
                    break;
                }
            }
            if(temp >= Math.sqrt(integer)){
                integerisprime = true;
            }
            //如果integer是prime且整除number，把integer设为最大prime
            if(integerisprime == true && number%integer == 0){
                largestprimefactor = integer;
            }
        }
        System.out.println(largestprimefactor);
    }
}
