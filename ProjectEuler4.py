largest = 0
num1 = 100
num2 = 100

def is_palindromic(temp):
    if temp < 100000:
        digit = 5
    else:
        digit = 6

    digits = []
    for i in range(0, digit):
        digits.append((temp%(10**(i + 1)))/(10**i))

    temp2 = 0
    for i in range(0, digit):
        if digits[i] == digits[digit - i - 1]:
            temp2 = i
        else:
            break

    if i == digit - 1:
        return True
    else:
        return False


for num1 in range(100, 1000):
    for num2 in range(100, 1000):
        temp = num1 * num2
        if is_palindromic(temp):
            largest = max(largest, temp)

print largest